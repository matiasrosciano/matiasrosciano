import { ADD_CITY } from '../actions'
import {RESETSTATE} from '../actions'


export const AddCity = (state = null,action) =>{
    switch (action.type){
        case ADD_CITY:
            console.log('addcity')
            return [...state,action.value]
        case RESETSTATE:
            console.log('RESET')
            return []
        default:
            return state
    }
}