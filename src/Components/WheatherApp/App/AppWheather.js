import React, { Component } from 'react';
import WeatherListContainer from './containers/WeatherListContainer'
import WeatherExtendsContainer from './containers/WeatherExtendsContainer'
import SearchWeatherContainer from './containers/SearchWeatherContainer'
import { Grid, Row, Col } from 'react-flexbox-grid'
import {withFirebase} from './../../../Components/Firebase'
import {connect} from 'react-redux'
import {Resetstate} from './actions'
import './App.css';




class AppWheather extends Component {

  componentWillUnmount(){
    console.log('cwumapp')
    this.props.dispatchResetstate()
  }

  render() {
    return (
      <div className="App">
        <header style={{display:'flex', justifyContent:'space-around'}}>
          <span></span>
          <h1 style={{marginBottom:'1em'}}>Weather App</h1>
          <button onClick={()=>this.props.firebase.SignOut()} style={{borderRadius:'2em',height:'5vh',backgroundColor:'#4eb5f1',color:'white',paddingLeft:'1em',paddingRight:'1em'}}>log out</button>          
        </header>
        <Grid fluid>
          <Row>
            <Col sm={12} md={4}>
              <h2>Search City</h2>
              <SearchWeatherContainer />      
            </Col>
            <Col sm={12} md={4}>
              <div className="WeatherList">
                <h2>List of cities</h2>
                <WeatherListContainer/>                   
              </div>
            </Col>
            <Col sm={12} md={4}>
                <h2>Extended weather</h2>
                <WeatherExtendsContainer/>              
            </Col>
          </Row>              
        </Grid>
      </div>
        
    )
  }
}

const mapDispatchToProps = dispatch => ({
  dispatchResetstate: () => dispatch(Resetstate())
})



export default withFirebase(connect(null,mapDispatchToProps)(AppWheather));
