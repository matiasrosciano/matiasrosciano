import React, { Component } from 'react';
import { connect } from 'react-redux'
import { AddCity, setCity } from './../actions'
import SearchWeather from './../components/SearchWeather'
import {withFirebase} from './../../../Firebase'



class SearchWeatherContainer extends Component {

  componentDidMount(){
    this.fetchCities()
    console.log('searcmd')
  }

  fetchCities(){
    let currentUserUid = this.props.firebase.auth.currentUser.uid
    this.props.firebase.GetCities(currentUserUid)
    .then((res)=>{
      res.data().Cities.forEach(city=>{
        this.handleAddWeather(city)
      })
    }).catch(err=>console.log(err))
  }

    
  handleAddWeather = (city) =>{
    let NewWeather = {
      city
    }
    this.props.dispatchAddCity(NewWeather)    
  }

  handleOnSelectLocation = (city) => {    
    this.props.dispatchsetCity(city)
}




    render() {
        return (
            <SearchWeather 
              handleonaddtolist={this.handleAddWeather}
              handleOnSelectedInSearch={this.handleOnSelectLocation}  
            />
        );
    }
}

const mapDispatchToProps = dispatch => ({
    dispatchAddCity: value => dispatch(AddCity(value)),
    dispatchsetCity: value => dispatch(setCity(value))
})

export default withFirebase(connect(null,mapDispatchToProps)(SearchWeatherContainer));