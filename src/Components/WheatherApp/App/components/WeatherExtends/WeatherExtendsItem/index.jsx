import React from 'react'
import WeatherData from './../../WeatherLocation/WeatherData'
import './styles.css'




const WeatherExtendsItem = (props) => ( 
    <div className='weatherlocationcont'>
        <h3 style={{marginBottom:0}}>
            {props.Day}
            
        </h3>
        <WeatherData data={props.data}/>
    </div>
)

export default WeatherExtendsItem