import React from 'react'
import PropTypes from 'prop-types'

const Location = (props) => (
    <div className="locationcont"><h3 style={{marginBottom:0}}>{props.city}</h3></div>
)

Location.propTypes = {
    city: PropTypes.string.isRequired
}



export default Location