import React from 'react'
import {withauthed,WithAuthConsumer} from './../Authed'
import Login from './Login'
import { Provider } from 'react-redux'
import { store } from './App/store'
import AppWheather from './App/AppWheather'


const WheatherApp = props => {


    return(
        
            <WithAuthConsumer>
            {
                AuthUser=>{
                    return(
                        AuthUser? <div style={{marginTop:'4em'}}><Provider store={store}> <AppWheather/> </Provider> </div> : <Login/>
                    )
                }
            }
            </WithAuthConsumer>
    )
}

export default withauthed(WheatherApp)