import app from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

const config = {
    apiKey: "AIzaSyBxgRAB2JxTXwjFJy4Znmb4cjPVLMlOPZA",
    authDomain: "matiasrosciano-e1a5d.firebaseapp.com",
    databaseURL: "https://matiasrosciano-e1a5d.firebaseio.com",
    projectId: "matiasrosciano-e1a5d",
    storageBucket: "matiasrosciano-e1a5d.appspot.com",
    messagingSenderId: "234355993864",
    appId: "1:234355993864:web:840cb21731a89ca9f1d0bd"
}

class Firebase{
    constructor(){
        app.initializeApp(config)
        this.auth = app.auth()
        this.db = app.firestore()
        
    }

    //auth

    SignIn = (email,password) => this.auth.signInWithEmailAndPassword(email,password)
    SingUp = (email,password) => this.auth.createUserWithEmailAndPassword(email, password)

    SignOut = () => this.auth.signOut()

    //Offices

    addOffice = office => this.db.collection("Offices").add(office)
    updateOffice = (id,office) => this.db.collection('Offices').doc(id).update(office)
    deleteOffice = id => this.db.collection('Offices').doc(id).delete()
    getOffices = () => this.db.collection("Offices").get()

    //Orders

    getOrders = () => this.db.collection('Orders').get()
    createorder = order => this.db.collection('Orders').add(order)
    updateOrder = (id,data) => this.db.collection('Orders').doc(id).update(data)

    //cities

    user = uid => this.db.collection('Users').doc(uid).set({Cities:[]})
    GetCities = (uid) => this.db.collection('Users').doc(uid).get()

    AddCity = async (uid,city) =>{
        let ArrayCities = await this.GetCities(uid)
        ArrayCities = ArrayCities.data().Cities
        console.log(ArrayCities)
        ArrayCities.push(city)
        console.log(ArrayCities)
        this.db.collection('Users').doc(uid).update({Cities:ArrayCities})
    } 

}

export default Firebase

