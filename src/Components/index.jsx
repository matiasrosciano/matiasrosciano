import React from 'react'
import Grid from '@material-ui/core/Grid';

import NavBar from "./NavBar";
import Sections from './sections'
import Sysosoffice from './Sysos/Office'
import SysosOrders from './Sysos/Orders'
import WheatherApp from './WheatherApp'

import Register from './WheatherApp/Register'

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

const app = () =>{
    return(
       <Router> 
        <Grid>
            <NavBar/>
            <Switch>
                <Route path="/" exact component={Sections} />
                <Route path="/Sysosoffice" exact component={Sysosoffice}/>
                <Route path='/SysosOrders' exact component={SysosOrders}/>
                <Route path='/WheatherApp' exact component={WheatherApp}/>
                <Route path='/Register' exact component={Register}/>
            </Switch>
            
        </Grid>
        </Router>
      
    )
}

export default app