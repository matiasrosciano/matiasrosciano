import React from 'react';

import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import CssBaseline from '@material-ui/core/CssBaseline';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Slide from '@material-ui/core/Slide';
import Button from '@material-ui/core/Button';
import Hidden from '@material-ui/core/Hidden';


function HideOnScroll(props) {
  const { children, window } = props;
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({ target: window ? window() : undefined });

  return (
    <Slide appear={false} direction="down" in={!trigger}>
      {children}
    </Slide>
  );
}

HideOnScroll.propTypes = {
  children: PropTypes.element.isRequired,
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

const NavBar = (props)=>{
  return (
    <React.Fragment>
      <CssBaseline />
      <HideOnScroll {...props}>
        <AppBar style={styles.navbar}>
            <Typography variant="h6" style={{paddingLeft:'1em'}}>MR</Typography>
            <Hidden xsDown>
              <nav style={styles.buttonsnav}>
                
                <a href="/#welcome" style={{textDecoration:'none'}}>               
                  <Button style={Object.assign({},styles.buttonlink,styles.welcome)}>Welcome </Button>
                </a>
                
                <a href="/#portfolio" style={{textDecoration:'none'}}>           
                <Button style={Object.assign({},styles.buttonlink,styles.portfolio)}> Portfolio</Button>
               </a>
                
                <a href="/#aboutme" style={{textDecoration:'none'}}>
                <Button style={Object.assign({},styles.buttonlink,styles.aboutme)}>About Me</Button>                
                </a>
                <a href="/#hireme" style={{textDecoration:'none'}}>
                <Button style={Object.assign({},styles.buttonlink,styles.hireme)}>Hire Me</Button>                
                </a>
              
              </nav>
            </Hidden>
            
            <div style={{paddingRight:'1em'}}></div>
        </AppBar>
      </HideOnScroll>
    </React.Fragment>
  );
}

export default NavBar

const styles = {
  navbar:{
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#1D3866',
    height: '5vh',
    justifyContent: 'space-between'
  },
  buttonlink:{
    height: '100%',
    paddingLeft: '1rem',
    paddingRight: '1rem',
    width: '110px',
    color: 'white',
    borderRadius: '0'
  },
  buttonsnav:{
    height: '100%'
  },
  welcome:{
    backgroundColor: '#008FA4'
  },
  portfolio:{
    backgroundColor: '#00B99A'
  },
  aboutme:{
    backgroundColor: '#84DE80'
  },
  hireme:{
    backgroundColor: '#ff6767'
  }
}