import React from 'react'
import test from './Media/test.jpg'


const Coute = (props) =>{

    return(
        <div style={styles.coutediv}>
            <p style={styles.coute}>{props.qoute}</p>
            <p style={styles.couteAutor}>{props.autor}</p>
        </div>
    )

}

export default Coute

const styles = {
    coutediv:{
        display:'flex',
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'center',
        backgroundImage: `url(${test})`,
        height:'25vh',
        backgroundAttachment:'fixed',
        backgroundRepeat:'no-repeat',
        backgroundPosition:'50% 50%',
        backgroundSize:'cover',
        maxHeight:'100%'

    },
    coute:{
        color:'white',
        fontSize:'2.8em',
        fontStyle:'italic',
        fontWeight:'100',
        textAlign:'center',
        paddingLeft:'0.5em',
        paddingRight:'0.5em'
    },
    couteAutor:{
        color:'white',
        fontSize:'1.7em',
    }
}