import React from 'react'
import Home from './home'
import Grid from '@material-ui/core/Grid';
import AboutMe from './Aboutme'
import Portfolio from './Portfolio'
import Hireme from './Hireme'
import Coute from './Coute'

const Sections = () => {
    return(
        <Grid>
            <Home/>
            <Coute
                qoute="One machine can do the work of fifty ordinary men. No machine can do the work of one extraordinary man."
                autor='Elbert Hubbard'
            />
           
            <Portfolio/>
            <Coute
                qoute="People don't care about what you say, they care about what you build."
                autor='Mark Zuckerberg'
            />
            <AboutMe/>
            <Hireme/>
        </Grid>
        
    )
}

export default Sections