import React from 'react'
import Grid from '@material-ui/core/Grid';
import videocover from './Media/video.mp4'

const Home = () =>{
    return(
            <Grid style={{width:'100%',height:'100vh',position:'relative'}}>
            <video loop autoPlay muted style={styles.videoback}>
                <source src={videocover}/>
               
            </video>
            <Grid item xs={12} style={Object.assign({},styles.titletext,styles.left)}>
                <p style={{margin:'0'}}>Rosciano</p>
                <p style={{margin:'0'}}>Matias</p>
            </Grid>
            <Grid item xs={12} style={Object.assign({},styles.titletext,styles.right)} >
                <p style={{margin:'0'}}>React</p>
                <p style={{margin:'0'}}>Developer</p>
            </Grid>            
            </Grid>        
    )
}

export default Home

const styles = {
    videoback:{ 
        width: '100%',
        height:'100%',
        objectFit:'cover',
    },

    titletext:{
        color: 'white',
        position: 'absolute',
        fontSize: '60px',
        
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        fontFamily: 'Roboto',
    },
    left:{
        left:'5%',
        top: '20%',
    },
    right:{
        right:'5%',
        top:'45%'
    }

}