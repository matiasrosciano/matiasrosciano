import React from 'react'
import Container from '@material-ui/core/Container'
import avatar from './Media/avatar.jpg'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button';

import linkdin from './Media/linkedin.svg'
import skype from './Media/skype.svg'
import gmail from './Media/gmail.svg'
import gitlab from './Media/gitlab.svg'

import './styles.css'



const Hireme = () =>(
    <Container style={styles.container}>        
            <img src={avatar} alt="" style={styles.avatar}/>
            <Typography variant='body1' style={styles.name}>Matias Nicolas Rosciano</Typography>
            <Typography variant='body1' style={styles.mail}>matiasrosciano@gmail.com</Typography>
            <Button variant="contained" color='primary' style={styles.hireme}>HIRE ME </Button>
            <Container style={styles.socialbuttons}>                
                <a href="http://www.linkedin.com/in/matias-rosciano-405116175" target='_blank' rel="noopener noreferrer"><Typography variant='body2' ><img src={linkdin} alt="" style={styles.icons}/></Typography></a>
                <a href="skype:matias_rosciano?chat"  target='_blank' rel="noopener noreferrer"><Typography variant='body2' ><img src={skype} alt="" style={styles.icons}/></Typography></a>
                <a href="mailto:matiasrosciano@gmail.com"><Typography variant='body2' ><img src={gmail} alt="" style={styles.icons}/></Typography></a>
                <a href="http://gitlab.com/matiasrosciano" target='_blank' rel="noopener noreferrer"><Typography variant='body2' ><img src={gitlab} alt="" style={styles.icons}/></Typography></a>
            </Container>
    </Container>
)

const styles = {
    container:{
        backgroundColor: '#f8f8f8',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        height: '100vh'
    },
    avatar:{
        borderRadius: '200px',
        width: '189px',
        height: '189px',
        border: '0.2em solid',
        borderColor: '#41969c'

    },
    name:{
        fontSize: '2em',
        fontFamily: 'roboto',
        fontWeight: '200',
    },
    mail:{
        fontSize: '1.2em',
        fontFamily: 'roboto',
        fontWeight: '300',
    },
    hireme:{
        marginTop: '1.5em',
        paddingLeft: '1rem',
        paddingRight: '1rem',
        width: '110px',
        color: 'white',
        borderRadius: '0',
        backgroundColor: '#ff6767',
    },
    socialbuttons:{
        marginTop:'2em',
        display:'flex',
        flexDirection:'row',
        justifyContent:'center'
    },
    icons:{
        width: '3em',
        height: '3em',
        margin:'1em'
    }
}



export default Hireme