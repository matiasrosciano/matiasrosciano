import React from 'react'
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography'
import Works from './../Works'
import Container from '@material-ui/core/Container'


const Portfolio = () => {



 return(
    <Container maxWidth="lg">
  <Grid container style={styles.port}>
   <Grid item xs={12} md={4} id="portfolio">
    <Typography variant='h2' style={styles.title}>
     Portfolio
    </Typography>
   </Grid>
   <Grid item xs={12} md={8}>
    <Typography variant='body1' style={styles.paragraph}>
    I share with you part of my work, desingned and developed by myself I made some extraction of components in order to make you interact with the ux design and performance of the development My work includes: development with ReactJS , integration with RestFull Api’s or Integration with the platform Firebase and their products ( RealTime DataBase, Authentication, Cloud, Etc). In fact This web is hosted in firebase. implementation FullResponsive with Material-ui or Twitter Bootstrap also Wordspress development
    </Typography>
   </Grid>
  </Grid>
  <Works/>
  
  </Container>
 )
}

export default Portfolio

const styles = {
 port:{
  backgroundColor: '#f8f8f8',
  padding: '2rem'
 },
 title:{
  borderBottom:'#00B99A solid 0.2rem',
  borderLeft:'#00B99A solid 0.4rem',
  width:'fit-content',
  letterSpacing:'0.16rem',
  paddingLeft: '0.2em',
  marginBottom:'2rem'
 },
 paragraph:{
  fontSize: '24px',
  lineHeight:'2',
  fontWeight:'lighter'
 }
}