import React from 'react'
import Typography from '@material-ui/core/Typography'
import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid';
import about from './Media/about.jpg'

const AboutMe = () =>{

 return(
    <div style={styles.AboutContainer}>
    <Container maxWidth="lg" style={styles.container}> 
        <Grid>
            <Typography style={styles.title} variant="h2">About Me</Typography> 
        </Grid>
        <Grid style={styles.experience}>
            <Grid style={styles.leftabout}>
            </Grid>
                <Grid>
                    <Typography variant='h3' style={styles.subtitle}>
                        Experiencie
                    </Typography>
                    <article style={styles.work}>
                        <span style={styles.circle}></span>
                        <Typography variant='body1'>
                            January 2019 - Now
                        </Typography>
                        <div style={styles.line}></div>
                        <Typography variant='h4' style={styles.worksTitle}>
                            Front-End Developer
                        </Typography>
                        <Typography variant='h6' style={styles.worksEmplo}>
                            Entropy Development Center
                        </Typography>
                        <p style={styles.description}>development a web app to improve communication between producers and trucks owners for the transport of products</p>                        
                        <ul style={styles.list}>
                            <li>ReactJs</li>
                            <li>JavaScript ES6</li>
                            <li>Context</li>
                            <li>Firebase
                                <ul style={styles.sublist}>
                                    <li>Authentication</li>
                                    <li>CloudFirestore</li>
                                    <li>Hosting</li>
                                </ul>
                            </li>
                        </ul>
                        
                    </article>
                    <article style={styles.work}>
                        <span style={styles.circle}></span>
                        <Typography variant='body1'>
                            June 2019 - Now
                        </Typography>
                        <div style={styles.line}></div>
                        <Typography variant='h4' style={styles.worksTitle}>
                            Front-End Developer
                        </Typography>
                        <Typography variant='h6' style={styles.worksEmplo}>
                            Forty2
                        </Typography>
                        <p style={styles.description}>In charge of the front-end development of a web application for the health sector</p>                        
                        <ul style={styles.list}>
                            <li>UX/UI Design</li>
                            <li>ReactJs</li>
                            <li>JavaScript ES6</li>
                            <li>Redux</li>
                            <li>Bootstrap</li>                            
                        </ul>
                    </article>
                    <article style={styles.work}>
                        <span style={styles.circle}></span>
                        <Typography variant='body1'>
                            July 2018 - December 2018
                        </Typography>
                        <div style={styles.line}></div>
                        <Typography variant='h4' style={styles.worksTitle}>
                            Freelance Web Developer
                        </Typography>
                        <Typography variant='h6' style={styles.worksEmplo}>
                            Portal Motos
                        </Typography>
                        <p style={styles.description}>developed ecommerce web app</p>                        
                        <ul style={styles.list}>
                            <li>ReactJs</li>
                            <li>Bootstrap</li>
                            <li>JavaScript ES6</li>
                            <li>Material-ui</li>
                        </ul>
                    </article>
                    <article style={styles.work}>
                        <span style={styles.circle}></span>
                        <Typography variant='body1'>
                            January 2018 - June 2018
                        </Typography>
                        <div style={styles.line}></div>
                        <Typography variant='h4' style={styles.worksTitle}>
                            Freelance Developer
                        </Typography>
                        <Typography variant='h6' style={styles.worksEmplo}>
                            Motorcyle Workshop
                        </Typography>
                        <p style={styles.description}>Develop a desktop system to manage motorcycle workshops, dedicated to organizing the work done on motorcycles</p>      
                        <ul style={styles.list}>
                            <li>VB.net</li>
                            <li>Entity framework</li>
                            <li>Postgresql</li>
                        </ul>                  
                    </article>
                    
                </Grid>
            
        </Grid>
    </Container>
    </div>
 )
  
}

export default AboutMe

const styles = {
leftabout:{
    width:'100%',
    
},
AboutContainer:{
    backgroundImage:`url(${about})`,
    backgroundAttachment:'fixed',
    backgroundRepeat:'no-repeat',
    backgroundPosition:'50% 50%',
    backgroundSize:'cover',
    maxHeight:'100%'
},
container:{
  display: 'flex',
  flexDirection:'column',
  
 },
 title:{
  marginTop:'2rem',
  fontFamily:'Roboto',
  borderBottom:'#84DE80 solid 0.2rem',
  borderLeft:'#84DE80 solid 0.4rem',
  paddingLeft: '0.2em',
  width:'fit-content',
  letterSpacing:'0.16rem',
  marginBottom:'2rem'
 },
 subtitle:{
    marginTop:'2rem',
    fontFamily:'Roboto',
    letterSpacing:'0.16rem',
    marginBottom:'0',
    fontSize:'2em',
    paddingRight:'10em'
 },
 parrafo:{
  fontSize:'24px',
  fontFamily:"Lato",
  padding: '2rem',
  fontWeight: 300
 },
 worksTitle:{
    fontSize:'1.5em',
 },
 experience:{
     display:'flex',
     justifyContent:'space-between',
    
 },
 circle:{
    width: '34px',
    height: '34px',
    display: 'block',
    background: '#41969c',
    borderRadius: '34px',
    textAlign: 'center',
    color: '#fff',
    lineHeight: '31px',
    fontSize: '1.375em',
    fontWeight: '600',
    left: '-39px',
    top: '42px',
    position:'relative'
 },
 line:{
    height: '2px',
    border: 'none',
    margin: '5px 0 0',
    display: 'block',
    background: '#41969c',
    position: 'relative',
 },
 worksEmplo:{
    fontSize:'1em',
    fontWeight: '300',
    marginBottom: '0.3em'
 },
 list:{
     marginTop: '1em',
     listStyleType: 'none'
 },
 sublist:{
    listStyleType: 'none',
    paddingLeft:'1em'
 },
 work:{
     marginBottom: '2em'
 },
 description:{
     padding:'0 1em 0 1em',
     
 }
}