import React, { Component } from 'react';
import PaginationOrders from './PaginationOrders'
import PaginationNumbers from './PaginationNumbers'
import {withFirebase} from './../Firebase'


class SysosOrders extends Component {
    constructor(props) {
        super(props);
        
        let user = JSON.parse(localStorage.getItem('currentUser'));
    
        this.state = {
          currentUser: user,
          statusDescription: '',
          ordersAprobe:[],
          ordersToAprobe:[],
          ordersFiltersAprobe:[],
          ordersFiltersToAprobe:[],
          search: '',
          currentPage: 1,
          selectedorder: 'toaprobe',
          orderRejectId: null
    
        };
    
        this.getAllOrders = this.getAllOrders.bind(this);
        this.parseDate = this.parseDate.bind(this);
        this.handleApprove = this.handleApprove.bind(this);
        this.handleChange = this.handleChange.bind(this);
      }
    
      setCurrentPage = (page) =>{
        isNaN(page) ?
          page === '+' 
            ? this.setState({currentPage: this.state.currentPage+1})
            : this.setState({currentPage: this.state.currentPage-1})
        : this.setState({currentPage:page})
      }
    
      sortOrders(orders,flag){
    
        flag?
          orders.sort((a,b) => {
            let afecha = new Date(a.createdAt)
            let bfecha = new Date(b.createdAt)
            return (bfecha - afecha)
          })
        :
          orders.sort((a,b) => {
            let afecha = new Date(a.updatedAt)
            let bfecha = new Date(b.updatedAt)
            return (bfecha - afecha)
          })
    
      }
    
      setOrders(orders) {
        //this.sortOrders(orders)
        let aprobeOrders=[],toAprobeOrders=[]
        orders.forEach((order)=>{
          order.status?
            aprobeOrders.push(order)
          :
            toAprobeOrders.push(order)
        })
        this.sortOrders(toAprobeOrders,true)
        this.sortOrders(aprobeOrders,false)
        
        this.setState({ 
          ordersAprobe: aprobeOrders,
          ordersToAprobe: toAprobeOrders,
          ordersFiltersAprobe: aprobeOrders,
          ordersFiltersToAprobe: toAprobeOrders,
        });

      }

      componentDidMount(){
        // let date = new Date()
        // date = date.toISOString()
        // let neworder = {
        //   affiliateNumber: 598,
        //   fullName: 'Another User',
        //   personalId: 20546987,
        //   quantity: 9,
        //   createdAt: date,
        //   updatedAt: '',
        //   status: ''
        // }

        // this.props.firebase.createorder(neworder)
        //   .then(()=>this.getAllOrders())
        //   .catch(err=>console.log(err))
        this.getAllOrders()
      }
    
      getAllOrders = ()=>{
        let loadOrders = []
        this.props.firebase.getOrders()
          .then((orders)=>{
            orders.forEach(item=>{
              const {affiliateNumber, fullName, personalId, quantity, createdAt, updatedAt , status} = item.data()
              loadOrders.push({
                id: item.id,
                affiliateNumber,
                fullName,
                personalId,
                quantity,
                createdAt,
                updatedAt,
                status
              })
            })
            this.setOrders(loadOrders)
          })
        
        

        // var that = this;
        // orderService.getOrdersByHealthInsurance(this.state.currentUser.id)
        //   .then(
        //       orders => { 
        //         orders.reverse();
        //         that.setOrders(orders);
        //       },
        //       error => {
        //         /* eslint-disable no-console */
        //         console.log(error);
        //         /* eslint-enable no-console */
        //       }
        //   );
      }
    
      parseDate(date){
        let orderdate = new Date(date);
        let options = {year: "2-digit",month: "2-digit",day: "2-digit",hour: "2-digit",minute: "2-digit"}
        return orderdate.toLocaleDateString("es-ES",options);
      }
    
      handleApprove(orderId, approved, statusDescription){
        let updatedAt = new Date()
        updatedAt = updatedAt.toISOString()
        let data = {
          status : approved,
          statusDescription,
          updatedAt
        }
        

        this.props.firebase.updateOrder(orderId,data)
          .then(()=>this.getAllOrders())
          .catch(err=>console.log(err))


    
        // orderService.approve(orderId, approveData)
        //   .then(
        //     data => {
        //       console.log(data);
        //       this.getAllOrders();
        //     },
        //     error => {
        //       /* eslint-disable no-console */
        //       console.log(error);
        //       /* eslint-enable no-console */
        //     }
        //   )
      }
    
      handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
      }
    
      OnSearch = (e) => {
        let newFilter = e.target.value
        let regex = new RegExp(newFilter, 'i')
    
        switch (e.target.name){
          case 'orderToAprobeSearch':
            this.setState({
              ordersFiltersToAprobe: this.state.ordersToAprobe.filter( q => ( regex.test(q.fullName) || regex.test(q.personalId) || regex.test(q.affiliateNumber) ))
            })
            break
          case 'DateFilterToAprobe':
            this.setState({
              ordersFiltersToAprobe: this.state.ordersToAprobe.filter( q =>  regex.test(q.createdAt))
            })
            break
          case 'orderAprobeSearch':
            this.setState({
              ordersFiltersAprobe: this.state.ordersAprobe.filter( q => ( regex.test(q.fullName) || regex.test(q.personalId) || regex.test(q.affiliateNumber) ))
            })
            break
          case 'DateFilterAprobe':
            this.setState({
              ordersFiltersAprobe: this.state.ordersAprobe.filter( q => regex.test(q.createdAt))
            })
            break
          default:
            this.setState({
              ordersFiltersAprobe: this.state.ordersAprobe.filter( q => regex.test(q.status))
            })
            break
        }
      }
    
      handleSelectedCheck = (e) =>{
        this.setState({
          selectedorder: e.target.value
        })
      }
    
      handleRechaze = (orderid) =>{
        this.setState({
          orderRejectId: orderid
        })
      }
    
      render() {
        let objectsAprobe = this.state.ordersFiltersAprobe;
        let objectsToAprobe = this.state.ordersFiltersToAprobe
        console.log(objectsToAprobe)
        let that  = this;
    
        return (
          <div className='mt-5 pt-4 m-5 '>
            <input type="radio" id='toaprobe' name='selectorder' value='toaprobe' checked={this.state.selectedorder === 'toaprobe'} onChange={this.handleSelectedCheck} />
            <label className='mr-3' htmlFor="toaprobe">Autorizar</label>
            <input type="radio" id='aprobe' name='selectorder' value='aprobe' checked={this.state.selectedorder === 'aprobe'} onChange={this.handleSelectedCheck}/>
            <label htmlFor="aprobe">Autorizadas</label>
            
            {this.state.selectedorder === 'toaprobe'?
              <div >
                <h3 className="text-center mb-3">Autorizar Ordenes</h3>
                <input type="text" className="form-control" id="orderSearch" name="orderToAprobeSearch" onChange={this.OnSearch} placeholder='Escriba para buscar orden por nombre/dni/n° afiliado' style={{width:'50%', textAlign:'center',margin:'auto'}} />
                <div className='table-responsive'>
                <table className="table orders-table table-striped mx-auto w-auto">
                  <thead>
                    <tr>
                      <th className='text-center' scope="col">Nº De Afiliado</th>
                      <th className='text-center' scope="col">Nombre</th>
                      <th className='text-center' scope="col">Dni</th>
                      <th className='text-center' scope="col">Cantidad de Ordenes</th>
                      <th className='text-center' scope="col">Fecha Generación
                      <input type="date" name="DateFilterToAprobe" onChange={this.OnSearch}/>
                      </th>
                      <th scope="col"></th>
                      
                    </tr>
                  </thead>
                  <tbody>
                    {
                      objectsToAprobe.length > 0 ?
    
                      <PaginationOrders
                        orders={objectsToAprobe}
                        currentpage={this.state.currentPage}
                        toaprobeselected={true}
                        handleApprove={this.handleApprove}
                        handleRechaze={this.handleRechaze}
                      />
                      :
                      <tr><th className='text-center' colSpan="7">No se encontraron ordenes</th></tr>
                    }
                    {/* objectsToAprobe.map(function(object, i){
                      if(!object.status){
                        return (
                          <tr key={i}>
                            <td>{object.id}</td>
                            <td>{object.affiliateNumber}</td>
                            <td>{object.fullName}</td>
                            <td>{object.personalId}</td>
                            <td>{object.quantity}</td>
                            <td>{that.parseDate(object.createdAt)}</td>
                            <td>{object.status}</td>
                            <td className='d-flex'>
                              <button className="btn btn-info btn-sm d-inline" onClick={()=> that.handleApprove(object.id, true, null)}>Autorizar</button>                        
                              <button className="btn btn-danger btn-sm ml-1 d-inline" data-toggle="modal" data-target="#rejectOrder" onClick={()=>{orderRejectId = object.id}}>Rechazar</button>
                            </td>
                          </tr>
                        )}
                    })} */}
                  </tbody>
                </table>
                </div>
                <PaginationNumbers
                  ordersnumber={objectsToAprobe.length}
                  setcurrentpage={this.setCurrentPage}
                  currentPage={this.state.currentPage}
                />
                </div>
              :
              <div>
              <h3 className="text-center mb-3">Ordenes Autorizadas/Rechazadas</h3>
            <input type="text" className="form-control" id="orderSearch" name="orderAprobeSearch" onChange={this.OnSearch} placeholder='Escriba para buscar orden por nombre/dni/n° afiliado' style={{width:'50%', textAlign:'center',margin:'auto'}} />
            <div className='table-responsive'>
            <table className="table orders-table table-striped mx-auto w-auto">
              <thead>
                <tr>
                  <th className='text-center' scope="col">Nº de Afiliado</th>
                  <th className='text-center' scope="col">Nombre</th>
                  <th className='text-center' scope="col">Dni</th>
                  <th className='text-center' scope="col">Cantidad de Ordenes</th>
                  <th className='text-center' scope="col">Fecha de Aprobada/Rechazada
                  <input type="date" name="DateFilterAprobe" onChange={this.OnSearch}/>
                  </th> 
                  <th scope="col">Estado
                    <select name="Estado" onChange={this.OnSearch}>
                      <option value="">Elija un estado para filtrar</option>
                      <option value="Autorizada">Autorizadas</option>
                      <option value="Rechazada">Rechazadas</option>
                      <option value="Anulada">Anuladas</option>
                    </select>
                  </th>
                  <th scope="col"></th>
                </tr>
              </thead>
              <tbody>
                {
                  objectsAprobe.length > 0 ?
    
                  <PaginationOrders
                    orders={objectsAprobe}
                    currentpage={this.state.currentPage}
                    toaprobeselected={false}
                  />
                  :
                  <tr><th className='text-center' colSpan="7">No se encontraron ordenes</th></tr>
                }
              </tbody>
            </table>
            </div>
    
            <PaginationNumbers
              ordersnumber={objectsAprobe.length}
              setcurrentpage={this.setCurrentPage}              
              currentPage={this.state.currentPage}
            />
            </div>
            }
    
            {/* Validate Modal */}
            <div className="modal fade mt-5" id="rejectOrder" tabIndex="-1" role="dialog" aria-hidden="true">
              <div className="modal-dialog" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLabel">Rechazar Order</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="form-group">
                        <label>Descripción</label>
                        <textarea className="form-control" rows="3" name="statusDescription" id="statusDescription" value={this.state.statusDescription} onChange={this.handleChange}></textarea>
                      </div>
                    </form>
                  </div>
                  <div className="modal-footer">
                    <button type="button" className="btn btn-danger" data-dismiss="modal" onClick={()=> that.handleApprove(this.state.orderRejectId, false,this.state.statusDescription)}>Rechazar</button>
                  </div>
                </div>
              </div>
            </div>
            
            {/* Watch Order Modal */}
            <div className="modal fade" id="orderExample" tabIndex="-1" role="dialog" aria-hidden="true">
              <div className="modal-dialog" role="document">
                <div className="modal-content">
                  <div>
                    <img className="w-100" src="https://slack-imgs.com/?c=1&url=http%3A%2F%2Fwww.colmedsanjuan.com.ar%2Fhome%2Fimages%2Fconsultasinpapel01.png" alt='un alt' />
                  </div>
                  <div className="modal-footer">
                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                  </div>
                </div>
              </div>
            </div>
    
          </div>
        );
      }
    }    
export default withFirebase(SysosOrders);
