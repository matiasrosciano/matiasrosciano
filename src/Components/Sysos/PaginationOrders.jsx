import React from 'react'

const PaginationOrders = ({orders,currentpage,toaprobeselected,handleApprove,handleRechaze})=>{

    const ordersperpage = 10 
   
    const parseDate = (date)=>{
        let orderdate = new Date(date);
        let options = {year: "2-digit",month: "2-digit",day: "2-digit",hour: "2-digit",minute: "2-digit"}
        return orderdate.toLocaleDateString("es-ES",options);
    }

    const CurrentOrders = []
    const indexlastorder = currentpage * ordersperpage
    const indexfirstorder = indexlastorder - ordersperpage
    
    for (let index = indexfirstorder; index < indexlastorder; index++) {
        if (!orders[index]){break}
        CurrentOrders.push(orders[index])    
    }

    return(
        CurrentOrders.map((order)=>{
            return(
                <tr key={order.id}>
                        <td className='text-center'>{order.affiliateNumber}</td>
                        <td className='text-center'>{order.fullName}</td>
                        <td className='text-center'>{order.personalId}</td>
                        <td className='text-center'>{order.quantity}</td>
                        {toaprobeselected ? 
                            <td className='text-center'>{parseDate(order.createdAt)}</td>
                        :
                            <td className='text-center'>{parseDate(order.updatedAt)}</td>
                        }
                        
                    <td className='text-center'>{order.status}</td>
                {toaprobeselected ?
                    <td className='d-flex'>
                        <button className="btn btn-info btn-sm d-inline" onClick={()=> handleApprove(order.id, true, null)}>Autorizar</button>                        
                        <button className="btn btn-danger btn-sm ml-1 d-inline" data-toggle="modal" data-target="#rejectOrder" onClick={()=>handleRechaze(order.id)}>Rechazar</button>
                    </td>
                :
                    null
                }
                </tr>
                
            )
            
        })
    )
}

export default PaginationOrders
