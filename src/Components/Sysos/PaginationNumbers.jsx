import React,{Component} from 'react'

class PaginationNumbers extends Component{
    constructor(props){
        super(props)    

        this.state={
            ordersperpage: 10,
            MaxButtonsNumbers:5,
            InitialButton:2,
        }
    }

    ChangeinitialButton = async (pageNumber) =>{
        await this.props.setcurrentpage(pageNumber)
        const lastpage = Math.ceil(this.props.ordersnumber/this.state.ordersperpage)
        if (this.props.currentPage > 3 && this.props.currentPage < lastpage - 2 ){
            let newInitialButton = this.props.currentPage - 2
            this.setState({InitialButton: newInitialButton})
        }else{
            if(this.props.currentPage <= 3){
                let newInitialButton = 2
                this.setState({InitialButton:newInitialButton})
            }else{
                let newInitialButton = lastpage - 5 
                this.setState({InitialButton: newInitialButton})
            }
        }      
    }

    renderPaginationLarge = (NumbersOfPages) =>{      
        const currentpage = this.props.currentPage
        
        const pageNumber = []               
        let NumbersButtons =  NumbersOfPages > this.state.MaxButtonsNumbers ? this.state.MaxButtonsNumbers :  NumbersOfPages - 1
      
        for (let i = this.state.InitialButton; i < this.state.InitialButton+NumbersButtons; i++) {
            pageNumber.push(i)        
        }
        return(
            <nav className="Page navigation example">
                    <ul className="pagination justify-content-center">
                        <li key={'primero'} className={ currentpage === 1 ? 'page-item active' : 'page-item' }><button onClick={()=>{this.ChangeinitialButton(1)}} className="page-link">Primera</button></li>
                        <li key={'ant'} className={currentpage === 1? 'page-item disabled' : 'page-item'}><button className="page-link" onClick={()=>{this.ChangeinitialButton('-')}}> Ant  </button></li>
                        {pageNumber.map((number)=>{
                            return(
                                <li key={number} className={currentpage === number ? 'page-item active' : 'page-item'}>
                                    <button onClick={()=>{this.ChangeinitialButton(number)}} className="page-link">{number}</button>
                                </li>
                            )
                        })}
                        <li key={'sig'} className={currentpage === NumbersOfPages ? 'page-item disabled': 'page-item'}><button className="page-link" onClick={()=>{this.ChangeinitialButton('+')}}> Sig </button></li>
                        <li key={NumbersOfPages} className={currentpage === NumbersOfPages ? 'page-item active' : 'page-item'}><button onClick={()=>{this.ChangeinitialButton(NumbersOfPages)}} className="page-link">Ultima</button></li>
                    </ul>
            </nav>
        )
    }

    renderPaginationSmall = (NumbersOfPages) =>{
        const currentpage = this.props.currentPage
        const pageNumber = []

        for (let i = 1; i <= NumbersOfPages; i++) {
            pageNumber.push(i)            
        }
        

        return(
            <nav className="Page navigation example">
                <ul className="pagination justify-content-center">
                    {pageNumber.map(number=>{
                        return(
                            <li key={number} className={currentpage===number? 'page-item active' : 'page-item'}>
                                <button onClick={()=>this.props.setcurrentpage(number)} className='page-link'>{number}</button>
                            </li>
                        )
                    })}
                </ul>
            </nav>
        )
    }
    

    render(){
        const NumbersOfPages = Math.ceil(this.props.ordersnumber/this.state.ordersperpage)
        
        return(
            NumbersOfPages > this.state.MaxButtonsNumbers?
                this.renderPaginationLarge(NumbersOfPages)
            :
                this.renderPaginationSmall(NumbersOfPages)        
        )
    }
}
    

export default PaginationNumbers
