import React, { Component } from 'react';
import TimePicker from 'rc-time-picker';
import "rc-time-picker/assets/index.css";
import moment from 'moment'
import {withFirebase} from './../Firebase'

class Sysosoffice extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id:'',
      address: '',
      homePhone: '',
      homePhoneAreaCode: '',
      personalPhoneAreaCode: '',
      personalPhone: '',
      prov: '',
      city: '',
      schedules: {
        monday:{
          active:false
        },
        tuesday: {
          active:false
        },
        wednesday: {
          active:false
        },
        thursday: {
          active:false
        },
        friday: {
          active:false
        },
        saturday: {
          active:false
        }
      },
      offices: [],
      onUpdate: false            
    };

    this.handleChange = this.handleChange.bind(this);
    this.buildSchedules = this.buildSchedules.bind(this);
    this.removeOffice = this.removeOffice.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  checkDay = e =>{
    let newDaysState = {...this.state.schedules}
    newDaysState[e.target.name].active = e.target.checked
    this.setState({
      schedules : newDaysState
    })
  }

  buildSchedules(hour,day,value) {    
    let newSchedulesState = {...this.state.schedules}
    newSchedulesState[day][hour] = value && value.format('HH:mm')
    this.setState({
      schedules : newSchedulesState
    })  
  }

  loadData = () =>{
    let loadOffices = []
    this.props.firebase.getOffices().then((offices)=>{
      console.log(offices)
      offices.forEach((item)=>{
        const {address,homePhone,homePhoneAreaCode,personalPhone,personalPhoneAreaCode,prov,city,schedules} = item.data()
        loadOffices.push({
          id: item.id,
          address,
          homePhoneAreaCode,
          homePhone,
          personalPhoneAreaCode,
          personalPhone,
          prov,
          city,
          schedules
        })
      })
      this.setState({
        offices:loadOffices
      })
    })
  }

  componentDidMount() {
    //cargar offices de firebase
    this.loadData()
    
  }

  removeOffice(id){
  //remover office de firebase
    this.props.firebase.deleteOffice(id)
      .then(()=>this.loadData())
      .catch(err=>console.log(err))

  }  

  changueOffice = (object)=>{
    const { id, address,city,homePhone,homePhoneAreaCode,personalPhone,personalPhoneAreaCode,prov,schedules} = object
    this.setState({
      id,
      address,
      homePhone,
      homePhoneAreaCode,
      personalPhoneAreaCode,
      personalPhone,
      prov,
      city,
      schedules: JSON.parse(schedules),
      onUpdate: true
    })
  }

  handleSubmit(e) {
    e.preventDefault();

    let newoffice = {
      address: this.state.address,
      homePhone: this.state.homePhone,
      homePhoneAreaCode: this.state.homePhoneAreaCode,
      personalPhoneAreaCode: this.state.personalPhoneAreaCode,
      personalPhone: this.state.personalPhone,
      prov: this.state.prov,
      city: this.state.city,
      schedules: JSON.stringify(this.state.schedules)
    }
    let id = this.state.id

    console.log(newoffice)


    this.state.onUpdate?
      this.props.firebase
        .updateOffice(id,newoffice)
        .then(()=>{this.loadData();this.setState({onUpdate:false})})
        .catch(err=>console.log(err))
    :
      this.props.firebase
        .addOffice(newoffice)
        .then(()=>this.loadData())
        .catch(error=>console.log(error))

    this.resetForm()

    //update and create offices en firebase
  }

  resetForm = () =>{
    this.setState({
      address: '',
      homePhone: '',
      homePhoneAreaCode: '',
      personalPhoneAreaCode: '',
      personalPhone: '',
      prov: '',
      city: '',
      schedules: {
        monday:{
          active:false
        },
        tuesday: {
          active:false
        },
        wednesday: {
          active:false
        },
        thursday: {
          active:false
        },
        friday: {
          active:false
        },
        saturday: {
          active:false
        },
      },
    })
  }

  validate = (e) => {
    switch (e.target.name){
      case 'personalPhoneAreaCode':
      case 'personalPhone':
      case 'homePhoneAreaCode':
      case 'homePhone':
        if (!(e.charCode >= 48 && e.charCode <= 57)){
          e.preventDefault()
        }
        break;
      case 'city':
      case 'prov':
        if (!( (e.charCode >= 65 && e.charCode <= 122) || e.charCode === 32 )){
          e.preventDefault()
        }
        break;
      case 'address':
          if( !((e.charCode >= 65 && e.charCode <= 122) || (e.charCode >= 48 && e.charCode <= 57) || ( e.charCode === 32))) {
            e.preventDefault()
          }
        break;
      default:
    }    
  }

  render() {
    console.log('render')
    let offices = this.state.offices
    console.log(offices)
    return (
      <div className='mt-5'>
        <h3 className="text-center mb-3">Agregar Consultorios</h3>
        <form className="" onSubmit={this.handleSubmit}>
          <div className='d-flex'>
            <div className='ml-3 col-md-7'>
          <div className="form-group">
            <label htmlFor="addres">Dirección  <small className='required'>(*)</small></label>
            <input className="form-control" type="text" id="address" name="address"  onKeyPress={this.validate} value={this.state.address} onChange={this.handleChange} required />
          </div>
          <div className="form-group">
            <label htmlFor="phone">Telefono Principal <small className='required'>(*)</small><small> (Fijo o celular) </small></label>
            <div className="form-row">
              <div className="col-md-3">
                <input className="form-control" type="text" id="homePhoneAreaCode" name="homePhoneAreaCode"  onKeyPress={this.validate} value={this.state.homePhoneAreaCode} onChange={this.handleChange}  required />
              </div>          
              <div className="col-md-9">
                <input className="form-control" type="text" id="homePhone" name="homePhone"  onKeyPress={this.validate} value={this.state.homePhone} onChange={this.handleChange}  required />              
              </div>
            </div>
          </div>
          <div className="form-group">
            <label htmlFor="personalPhone">Telefono Secundario <small> (Fijo o celular) </small></label>
            <div className="form-row">
              <div className="col-md-3">
                <input type="text" className="form-control" id="personalPhoneAreaCode" name="personalPhoneAreaCode" onKeyPress={this.validate} value={this.state.personalPhoneAreaCode} onChange={this.handleChange}/>
              </div>
              <div className="col-md-9">
                <input type="tel" pattern="[0-9]{7}" className="form-control" id="personalPhone" name="personalPhone" onKeyPress={this.validate} value={this.state.personalPhone} onChange={this.handleChange} />
              </div>
            </div>
          </div>
          <div className="form-group">
            <label htmlFor="Provincia">Provincia  <small className='required'>(*)</small></label>
            <input type="text" className="form-control" id="prov" name="prov" value={this.state.prov} onChange={this.handleChange} required />
          </div>
          <div className="form-group">
            <label htmlFor="city">Ciudad  <small className='required'>(*)</small></label>
            <input type="text" className="form-control" id="city" name="city" value={this.state.city} onChange={this.handleChange} required />
          </div>
          </div>

          
          <div className="form-group col-md-5 d-flex flex-column mt-5">
            <h6 className='align-self-center mb-3'>Días y Horarios</h6>
 
            <div>
              <div className=""></div>
              <div className='d-flex' style={{justifyContent:'space-evenly'}}>
              <div className="text-center d-inline-block font-weight-bold">
                Mañana
              </div>
              <div className="text-center d-inline-block pl-0 font-weight-bold">
                Tarde
              </div>
              </div>
              <div className=""></div>
              <div className='d-flex' style={{justifyContent:'space-evenly'}}>
                <div></div>
              <div className="text-center d-inline-block font-weight-bold">
                Desde
              </div>
              <div className="text-center d-inline-block pl-0 font-weight-bold">
                Hasta
              </div>
              <div className="text-center d-inline-block font-weight-bold">
                Desde
              </div>
              <div className="text-center d-inline-block pl-0 font-weight-bold">
                Hasta
              </div>
              </div>
              <div className="form-check form-row d-flex" style={{justifyContent:'space-evenly'}}>
                <div className='col-1'>
                  <input id="mondayCheckbox" type="checkbox" name="monday" checked={this.state.schedules.monday.active} onChange={this.checkDay} className="form-check-input" />
                  <label htmlFor="mondayCheckbox" className="form-check-label">Lunes</label>
                </div>        
                    <TimePicker
                      style={{ width: 100 }}
                      showSecond={false}
                      onChange={this.buildSchedules.bind(this,'fromMorning','monday')}
                      minuteStep={30}
                      disabled={!this.state.schedules.monday.active}
                      value={this.state.schedules.monday.fromMorning ? moment(this.state.schedules.monday.fromMorning,'HHmm') : undefined}
                    />
                    <TimePicker
                        style={{ width: 100 }}
                        showSecond={false}
                        onChange={this.buildSchedules.bind(this,'toMorning','monday')}
                        minuteStep={30}
                        disabled={!this.state.schedules.monday.active}
                        value={this.state.schedules.monday.toMorning ? moment(this.state.schedules.monday.toMorning,'HHmm') : undefined}
                      />
                    <TimePicker
                        style={{ width: 100 }}
                        showSecond={false}
                        onChange={this.buildSchedules.bind(this,'fromAfternoon','monday')}
                        minuteStep={30}
                        disabled={!this.state.schedules.monday.active}
                        value={this.state.schedules.monday.fromAfternoon ? moment(this.state.schedules.monday.fromAfternoon,'HHmm') : undefined}
                      />
                    <TimePicker
                        style={{ width: 100 }}
                        showSecond={false}
                        onChange={this.buildSchedules.bind(this,'toAfternoon','monday')}
                        minuteStep={30}
                        disabled={!this.state.schedules.monday.active}
                        value={this.state.schedules.monday.toAfternoon ? moment(this.state.schedules.monday.toAfternoon,'HHmm') : undefined}
                      />
              </div>
              <div className="form-check form-row d-flex" style={{justifyContent:'space-evenly'}}>
                <div className='col-1'> 
                  <input id="tuesdayCheckbox" type="checkbox" name="tuesday" checked={this.state.schedules.tuesday.active} onChange={this.checkDay} className="form-check-input"/>
                  <label htmlFor="tuesdayCheckbox" className="form-check-label">Martes</label>
                </div>
                    <TimePicker
                      style={{ width: 100 }}
                      showSecond={false}
                      onChange={this.buildSchedules.bind(this,'fromMorning','tuesday')}
                      minuteStep={30}
                      disabled={!this.state.schedules.tuesday.active}
                      value={this.state.schedules.tuesday.fromMorning ? moment(this.state.schedules.tuesday.fromMorning,'HHmm') : undefined}
                    />
                    <TimePicker
                      style={{ width: 100 }}
                      showSecond={false}
                      onChange={this.buildSchedules.bind(this,'toMorning','tuesday')}
                      minuteStep={30}
                      disabled={!this.state.schedules.tuesday.active}
                      value={this.state.schedules.tuesday.toMorning ? moment(this.state.schedules.tuesday.toMorning,'HHmm') : undefined}
                    />
                    <TimePicker
                      style={{ width: 100 }}
                      showSecond={false}
                      onChange={this.buildSchedules.bind(this,'fromAfternoon','tuesday')}
                      minuteStep={30}
                      disabled={!this.state.schedules.tuesday.active}
                      value={this.state.schedules.tuesday.fromAfternoon ? moment(this.state.schedules.tuesday.fromAfternoon,'HHmm') : undefined}
                    />
                    <TimePicker
                      style={{ width: 100 }}
                      showSecond={false}
                      onChange={this.buildSchedules.bind(this,'toAfternoon','tuesday')}
                      minuteStep={30}
                      disabled={!this.state.schedules.tuesday.active}
                      value={this.state.schedules.tuesday.toAfternoon ? moment(this.state.schedules.tuesday.toAfternoon,'HHmm') : undefined}
                    />
              </div>
              <div className="form-check form-row d-flex" style={{justifyContent:'space-evenly'}}>
                <div className='col-1'>
                  <input id="wednesdayCheckbox" type="checkbox" name="wednesday" checked={this.state.schedules.wednesday.active} onChange={this.checkDay} className="form-check-input"/>
                  <label htmlFor="wednesdayCheckbox" className="form-check-label">Miercoles</label>
                </div>
                    <TimePicker
                      style={{ width: 100 }}
                      showSecond={false}
                      onChange={this.buildSchedules.bind(this,'fromMorning','wednesday')}
                      minuteStep={30}
                      disabled={!this.state.schedules.wednesday.active}
                      value={this.state.schedules.wednesday.fromMorning ? moment(this.state.schedules.wednesday.fromMorning,'HHmm') : undefined}
                    />
                    <TimePicker
                      style={{ width: 100 }}
                      showSecond={false}
                      onChange={this.buildSchedules.bind(this,'toMorning','wednesday')}
                      minuteStep={30}
                      disabled={!this.state.schedules.wednesday.active}
                      value={this.state.schedules.wednesday.toMorning ? moment(this.state.schedules.wednesday.toMorning,'HHmm') : undefined}
                    />
                    <TimePicker
                      style={{ width: 100 }}
                      showSecond={false}
                      onChange={this.buildSchedules.bind(this,'fromAfternoon','wednesday')}
                      minuteStep={30}
                      disabled={!this.state.schedules.wednesday.active}
                      value={this.state.schedules.wednesday.fromAfternoon ? moment(this.state.schedules.wednesday.fromAfternoon,'HHmm') : undefined}
                    />
                    <TimePicker
                      style={{ width: 100 }}
                      showSecond={false}
                      onChange={this.buildSchedules.bind(this,'toAfternoon','wednesday')}
                      minuteStep={30}
                      disabled={!this.state.schedules.wednesday.active}
                      value={this.state.schedules.wednesday.toAfternoon ? moment(this.state.schedules.wednesday.toAfternoon,'HHmm') : undefined}
                    />
              </div>

              <div className="form-check form-row d-flex" style={{justifyContent:'space-evenly'}}>
                <div className='col-1'>
                  <input id="thursdayCheckbox" type="checkbox" name="thursday" checked={this.state.schedules.thursday.active} onChange={this.checkDay} className="form-check-input"/>
                  <label htmlFor="thursdayCheckbox" className="form-check-label">Jueves</label>
                </div>

                    <TimePicker
                      style={{ width: 100 }}
                      showSecond={false}
                      onChange={this.buildSchedules.bind(this,'fromMorning','thursday')}
                      minuteStep={30}
                      disabled={!this.state.schedules.thursday.active}
                      value={this.state.schedules.thursday.fromMorning ? moment(this.state.schedules.thursday.fromMorning,'HHmm') : undefined}
                    />
                    <TimePicker
                      style={{ width: 100 }}
                      showSecond={false}
                      onChange={this.buildSchedules.bind(this,'toMorning','thursday')}
                      minuteStep={30}
                      disabled={!this.state.schedules.thursday.active}
                      value={this.state.schedules.thursday.toMorning ? moment(this.state.schedules.thursday.toMorning,'HHmm') : undefined}
                    />
                    <TimePicker
                      style={{ width: 100 }}
                      showSecond={false}
                      onChange={this.buildSchedules.bind(this,'fromAfternoon','thursday')}
                      minuteStep={30}
                      disabled={!this.state.schedules.thursday.active}
                      value={this.state.schedules.thursday.fromAfternoon ? moment(this.state.schedules.thursday.fromAfternoon,'HHmm') : undefined}
                    />
                    <TimePicker
                      style={{ width: 100 }}
                      showSecond={false}
                      onChange={this.buildSchedules.bind(this,'toAfternoon','thursday')}
                      minuteStep={30}
                      disabled={!this.state.schedules.thursday.active}
                      value={this.state.schedules.thursday.toAfternoon ? moment(this.state.schedules.thursday.toAfternoon,'HHmm') : undefined}
                    />
              </div>


              <div className="form-check form-row d-flex" style={{justifyContent:'space-evenly'}}>
                <div className='col-1'>
                  <input id="fridayCheckbox" type="checkbox" name="friday" checked={this.state.schedules.friday.active} onChange={this.checkDay} className="form-check-input"/>
                  <label htmlFor="fridayCheckbox" className="form-check-label">Viernes</label>
                </div>
                    <TimePicker
                      style={{ width: 100 }}
                      showSecond={false}
                      onChange={this.buildSchedules.bind(this,'fromMorning','friday')}
                      minuteStep={30}
                      disabled={!this.state.schedules.friday.active}
                      value={this.state.schedules.friday.fromMorning ? moment(this.state.schedules.friday.fromMorning,'HHmm') : undefined}
                    />
                    <TimePicker
                      style={{ width: 100 }}
                      showSecond={false}
                      onChange={this.buildSchedules.bind(this,'toMorning','friday')}
                      minuteStep={30}
                      disabled={!this.state.schedules.friday.active}
                      value={this.state.schedules.friday.toMorning ? moment(this.state.schedules.friday.toMorning,'HHmm') : undefined}
                    />
                    <TimePicker
                      style={{ width: 100 }}
                      showSecond={false}
                      onChange={this.buildSchedules.bind(this,'fromAfternoon','friday')}
                      minuteStep={30}
                      disabled={!this.state.schedules.friday.active}
                      value={this.state.schedules.friday.fromAfternoon ? moment(this.state.schedules.friday.fromAfternoon,'HHmm') : undefined}
                    />
                    <TimePicker
                      style={{ width: 100 }}
                      showSecond={false}
                      onChange={this.buildSchedules.bind(this,'toAfternoon','friday')}
                      minuteStep={30}
                      disabled={!this.state.schedules.friday.active}
                      value={this.state.schedules.friday.toAfternoon ? moment(this.state.schedules.friday.toAfternoon,'HHmm') : undefined}
                    />
              </div>


              <div className="form-check form-row d-flex" style={{justifyContent:'space-evenly'}}>
                <div className='col-1'>
                  <input id="saturdayCheckbox" type="checkbox" name="saturday" checked={this.state.schedules.saturday.active} onChange={this.checkDay} className="form-check-input"/>
                  <label htmlFor="saturdayCheckbox" className="form-check-label">Sábado</label>
                </div>
                    <TimePicker
                      style={{ width: 100 }}
                      showSecond={false}
                      onChange={this.buildSchedules.bind(this,'fromMorning','saturday')}
                      minuteStep={30}
                      disabled={!this.state.schedules.saturday.active}
                      value={this.state.schedules.saturday.fromMorning ? moment(this.state.schedules.saturday.fromMorning,'HHmm') : undefined}
                    />
                    <TimePicker
                      style={{ width: 100 }}
                      showSecond={false}
                      onChange={this.buildSchedules.bind(this,'toMorning','saturday')}
                      minuteStep={30}
                      disabled={!this.state.schedules.saturday.active}
                      value={this.state.schedules.saturday.toMorning ? moment(this.state.schedules.saturday.toMorning,'HHmm') : undefined}
                    />
                    <TimePicker
                      style={{ width: 100 }}
                      showSecond={false}
                      onChange={this.buildSchedules.bind(this,'fromAfternoon','saturday')}
                      minuteStep={30}
                      disabled={!this.state.schedules.saturday.active}
                      value={this.state.schedules.saturday.fromAfternoon ? moment(this.state.schedules.saturday.fromAfternoon,'HHmm') : undefined}
                    />
                    <TimePicker
                      style={{ width: 100 }}
                      showSecond={false}
                      onChange={this.buildSchedules.bind(this,'toAfternoon','saturday')}
                      minuteStep={30}
                      disabled={!this.state.schedules.saturday.active}
                      value={this.state.schedules.saturday.toAfternoon ? moment(this.state.schedules.saturday.toAfternoon,'HHmm') : undefined}
                    />
              </div>
            </div>
            <div className="text-center form-group mt-3">           
                <button type='submit' className="btn btn-dark">{this.state.onUpdate?'Guardar':'Agregar Consultorios'}</button>
            </div>
          </div>
          </div>
          
        </form>
        <div className="col my-4">
          <h3 className="text-center mb-3">Consultorios</h3>
          <table className="table orders-table table-striped">
            <thead>
              <tr className='row'>
                <th className='col' scope="col">Dirección</th>
                <th  className='col' scope="col">Teléfono Fijo</th>
                <th  className='col' scope="col">Teléfono Celular</th>
                <th  className='col' scope="col">Días y Horarios</th>
                <th  className='col' scope="col"></th>
              </tr>
            </thead>
            <tbody>
              {
                
                offices.map((object, i) => {
                  let schedules = JSON.parse(object.schedules)
                  return (
                    <tr className='row' key={i}>
                      <td className='col'>{object.address}</td>
                      <td className='col'>{object.homePhoneAreaCode} {object.homePhone}</td>
                      <td className='col'>{object.personalPhoneAreaCode} {object.personalPhone}</td>
                      <td className='col'>
                        {schedules.monday.active? <div>Lunes de {schedules.monday.fromMorning} a {schedules.monday.toMorning} - de {schedules.monday.fromAfternoon} a {schedules.monday.toAfternoon}</div>: null}
                        {schedules.tuesday.active? <div>Martes de {schedules.tuesday.fromMorning} a {schedules.tuesday.toMorning} - de {schedules.tuesday.fromAfternoon} a {schedules.tuesday.toAfternoon}</div>: null}
                        {schedules.wednesday.active? <div>Miercoles de {schedules.wednesday.fromMorning} a {schedules.wednesday.toMorning} - de {schedules.wednesday.fromAfternoon} a {schedules.wednesday.toAfternoon}</div>: null}
                        {schedules.thursday.active? <div>Jueves de {schedules.thursday.fromMorning} a {schedules.thursday.toMorning} - de {schedules.thursday.fromAfternoon} a {schedules.thursday.toAfternoon}</div>: null}
                        {schedules.friday.active? <div>Viernes de {schedules.friday.fromMorning} a {schedules.friday.toMorning} - de {schedules.friday.fromAfternoon} a {schedules.friday.toAfternoon}</div>: null}
                        {schedules.saturday.active? <div>Sabado de {schedules.saturday.fromMorning} a {schedules.saturday.toMorning} - de {schedules.saturday.fromAfternoon} a {schedules.saturday.toAfternoon}</div>: null}
                      </td>                 
                      <td className='col'>
                        <button className="btn btn-info btn-sm mr-sm-2" onClick={()=>this.changueOffice(object)}>Editar</button>
                        <button className="btn btn-info btn-sm" onClick={this.removeOffice.bind(this, object.id)}>Eliminar</button>
                      </td>
                    </tr>
                  )
              })}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}



export default withFirebase(Sysosoffice);
