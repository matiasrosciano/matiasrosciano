import React from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {Link} from 'react-router-dom'
import sysos from './media/SysOs.jpg'

const Sysos2 = () =>(
 <Card className="Proyect-card" style={styles.card}>
      <CardMedia
          style={styles.media}
          image={sysos}
          title="Sysos Orders"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            Sysos
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            This is other component from sysos, in this case is a table con buscador por varios campos, paginacion etc
          </Typography>
        </CardContent>
      <CardActions>
        <Link  to='/SysosOrders'>
        <Button size="small" color="primary">
          TRY UX
        </Button>        
        </Link>
      </CardActions>
    </Card>
)

export default Sysos2

const styles = {
 card: {
  maxWidth: 345,
  margin: '1rem'
},
media: {
  height: 140,
},
}