import React from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import entropy from './media/EntropyVideo.mp4'

const Entropy = () =>{

  const playVideo = () =>{
    document.getElementById('entropyvideo').play()
  }
  const stopVideo = () =>{
    document.getElementById('entropyvideo').pause()
  }

  return(
    <Card className="Proyect-card" style={styles.card} onMouseOver={()=>playVideo()} onMouseOut={()=>stopVideo()}>
        <CardMedia>
          <video id='entropyvideo' muted height='140'>
            <source src={entropy} />
          </video>
        </CardMedia>
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            Entropy
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            This site in wordpress 
          </Typography>
        </CardContent>
      <CardActions>
        <Button size="small" color="primary">
          <a href="http://entropyteam.com/" target='_blank' rel="noopener noreferrer">visit</a>          
        </Button>
      </CardActions>
    </Card>
    )
}

export default Entropy

const styles = {
 card: {
  maxWidth: 345,
  margin: '1rem',
},
media: {
  maxHeight: 140,
},
}