import React from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import wheather from './media/Wheather.mp4'
import {Link} from 'react-router-dom'

const WheatherApp = () =>{
  
  const playVideo = () =>{
    document.getElementById('wheathervideo').play()
  }
  const stopVideo = () =>{
    document.getElementById('wheathervideo').pause()
  }  
  
  return(
    <Card className="Proyect-card" style={styles.card} onMouseOver={()=>playVideo()} onMouseOut={()=>stopVideo()}>
            <CardMedia>
              <video id='wheathervideo' muted height='140' width='100%'>
                <source src={wheather} />
              </video>
            </CardMedia>
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                WheatherApp
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
                this is my first app, i add some few stuff for you can use. you can sign up and add any city to know their wheather
              </Typography>
            </CardContent>
          <CardActions>
          <Link  to='/WheatherApp'> 
            <Button size="small" color="primary" >
              FREE APP
            </Button>
          </Link>  
          </CardActions>
        </Card>
  )
}

export default WheatherApp

const styles = {
 card: {
  maxWidth: 345,
  margin: '1rem'
},
media: {
  height: 140,
},
}