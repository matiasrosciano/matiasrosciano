import React from 'react'
import Grid from '@material-ui/core/Grid'
import Sysos1 from './sysos1'
import Sysos2 from './sysos2'
import WheatherApp from './WheatherApp'
import Entropy from './Entropy'
import './styles.css'
import Slider from 'react-styled-carousel';


const Works = () =>{

  const responsive = [
    { breakPoint: 1280, cardsToShow: 2 }, // this will be applied if screen size is greater than 1280px. cardsToShow will become 4.
    { breakPoint: 760, cardsToShow: 1 },
  ];
  return(
 <Grid container style={styles.works}>
   <Slider responsive={responsive} showArrows={false} style={{paddingLeft:'4em',paddingRight:'4em'}}>
    <WheatherApp/>
    <Sysos1/>
    <Entropy/>
    <Sysos2/>
   </Slider>
 </Grid>
  )
}

export default Works

const styles = {
 works:{
  display:'flex'
 }
}