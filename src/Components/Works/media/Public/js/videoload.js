// Video Load
//  Valentin Rosciano / Gerardo Rosciano

window.addEventListener('load', eventWindowLoaded, false);
function eventWindowLoaded() {
   var videoElement = document.getElementById("thevideo");

   videoElement.addEventListener('progress',updateLoadingStatus,false);
   videoElement.addEventListener('canplaythrough',playVideo,false);

}

function updateLoadingStatus() {

   var loadingStatus = document.getElementById("loadingStatus");
   var videoElement = document.getElementById("thevideo");
   var percentLoaded = parseInt(((videoElement.buffered.end(0) /
      videoElement.duration) * 101));
    document.getElementById("loadingStatus").innerHTML =   percentLoaded + '%';
    if(percentLoaded>98){
        playVideo();
    }
}

function playVideo() {
   var videoElement = document.getElementById("thevideo");
   videoElement.play();

}