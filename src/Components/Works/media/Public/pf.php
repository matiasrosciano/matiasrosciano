<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Diego Valentin Rosciano</title>
    <meta name="description" content="Web, Mobile, Design, Front-end, Argentina, Freelancer, Responsive, ios, App, html, css, xCode, Objective-C, Bootstrap, Wordpress, JavaScript">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,100italic,300italic,400italic,700italic' rel='stylesheet' type='text/css'>
	
    <!-- ##### ICONS ##### -->
        
        <link rel="shortcut icon" href="icos/favicon.ico">
        <link rel="apple-touch-icon" sizes="57x57" href="icos/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="114x114" href="icos/apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="72x72" href="icos/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="144x144" href="icos/apple-touch-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="60x60" href="icos/apple-touch-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="120x120" href="icos/apple-touch-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="76x76" href="icos/apple-touch-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="152x152" href="icos/apple-touch-icon-152x152.png">
        <link rel="icon" type="image/png" href="icos/favicon-196x196.png" sizes="196x196">
        <link rel="icon" type="image/png" href="icos/favicon-160x160.png" sizes="160x160">
        <link rel="icon" type="image/png" href="icos/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="icos/favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="icos/favicon-32x32.png" sizes="32x32">
        <meta name="msapplication-TileColor" content="#2d89ef">
        <meta name="msapplication-TileImage" content="icos/mstile-144x144.png">
        <meta name="msapplication-config" content="icos/browserconfig.xml">
        
        <!-- ##### ICONS ##### -->
    
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/portfoliostyles.css">
    
    <script src = "http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src="../js/scrollview.js"></script>
    
    <!-- Google Analitycs -->
       <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-40628767-2', 'valentinrosciano.com');
  ga('send', 'pageview');

</script>
    
</head>
<body>
	
    <!-- Preloader 
    <div id="preloader" class="hidden-xs hidden-sm" style=" z-index:9999;">
        <div id="status">
            <div class="spinner">
                  <div class="dot1"></div>
                  <div class="dot2"></div>
            </div>
            <div class="preload_text_cont"><span class="preload_text">Loading..&nbsp;<span class="preload_text" id="loadingsite"></span></div>
        </div>
    </div>-->
    <div id="#thetop"></div>
	<?php
    $page = "1";
    switch($_GET['page']){
        case "2":
            $page = "2";
            break;
        case "3":
            $page = "3";
            break;
        case "4":
            $page = "4";
            break;
        case "5":
            $page = "5";
            break;
        case "6":
            $page = "6";
            break;
		case "7":
            $page = "7";
            break;
		case "8":
            $page = "8";
            break;
		case "9":
            $page = "9";
            break;
		case "10":
            $page = "10";
            break;
		case "11":
            $page = "11";
            break;
		case "12":
            $page = "12";
            break;
    }
    $page.=".html";
    require($page);
    ?>
    
    
    <section class="bnav" style="z-index:2000;">
    	<div class="bnav_text">
                <div class="hidden-xs">
                	<button type="button" class="btn btn-link pull-left" onclick="window.location.href='../portfolio.html'"><span class="entypo-briefcase" style="padding-right: 20px;"></span>BACK PORTFOLIO</button>
                    <button type="button" class="btn btn-link pull-left" onclick="window.location.href='../index.html'"><span class="entypo-home" style="padding-right: 20px;"></span>HOMEPAGE</button>
                    <div class="pull-left linevertical"></div>
                    <button type="button" class="btn btn-info pull-right" onClick="location.href='mailto:valentinrosciano77@gmail.com'"><span class="entypo-user-add" style="padding-right: 20px;"></span>HIRE ME</button>
                </div>
                <div class="visible-xs">
                	<button type="button" class="btn btn-link pull-left" onclick="window.location.href='../portfolio.html'"><span class="entypo-briefcase"></span></button>
                    <button type="button" class="btn btn-link pull-left" onclick="window.location.href='../index.html'"><span class="entypo-home"></span></button>
                    <div class="pull-left linevertical"></div>
                    <button type="button" class="btn btn-info pull-right" onclick="window.location.href='mailto:valentinrosciano77@gmail.com'"><span class="entypo-user-add"></span></button>
                </div>
            </div>
    </section>
      
    
    <script src="../js/bootstrap.min.js"></script>
   
    
    <!-- Preloader -->
	<script type="text/javascript">
        //<![CDATA[
            $(window).load(function() { // makes sure the whole site is loaded
                $('#status').fadeOut(); // will first fade out the loading animation
                $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
                $('body').delay(350).css({'overflow':'visible'});
            })
        //]]>
    </script>
    
</body>
</html>
