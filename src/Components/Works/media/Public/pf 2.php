<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Diego Valentin Rosciano</title>
    <meta name="description" content="Web, Mobile, Design, Front-end, Argentina, Freelancer, Responsive, ios, App, html, css, xCode, Objective-C, Bootstrap, Wordpress, JavaScript">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,100italic,300italic,400italic,700italic' rel='stylesheet' type='text/css'>
	
    <!-- ##### ICONS ##### -->
        
        <link rel="shortcut icon" href="icos/favicon.ico">
        <link rel="apple-touch-icon" sizes="57x57" href="icos/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="114x114" href="icos/apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="72x72" href="icos/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="144x144" href="icos/apple-touch-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="60x60" href="icos/apple-touch-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="120x120" href="icos/apple-touch-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="76x76" href="icos/apple-touch-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="152x152" href="icos/apple-touch-icon-152x152.png">
        <link rel="icon" type="image/png" href="icos/favicon-196x196.png" sizes="196x196">
        <link rel="icon" type="image/png" href="icos/favicon-160x160.png" sizes="160x160">
        <link rel="icon" type="image/png" href="icos/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="icos/favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="icos/favicon-32x32.png" sizes="32x32">
        <meta name="msapplication-TileColor" content="#2d89ef">
        <meta name="msapplication-TileImage" content="icos/mstile-144x144.png">
        <meta name="msapplication-config" content="icos/browserconfig.xml">
        
        <!-- ##### ICONS ##### -->
    
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/portfoliostyles.css">
    
    <script src = "http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src="../js/scrollview.js"></script>
</head>
<body>
	
    <!-- Preloader -->
    <div id="preloader" class="hidden-xs hidden-sm" style=" z-index:9999;">
        <div id="status">
            <div class="spinner">
                  <div class="dot1"></div>
                  <div class="dot2"></div>
            </div>
            <div class="preload_text_cont"><span class="preload_text">Loading:&nbsp;<span class="preload_text" id="loadingsite"></span></div>
        </div>
    </div>
    <div id="#thetop"></div>
	<?php
    $page = "1";
    switch($_GET['page']){
        case "2":
            $page = "2";
            break;
        case "3":
            $page = "3";
            break;
        case "4":
            $page = "4";
            break;
        case "5":
            $page = "5";
            break;
        case "6":
            $page = "6";
            break;
    }
    $page.=".html";
    require($page);
    ?>
    
    
    <section class="bnav" style="z-index:2000;" data-0="bottom:-100px;" data-300="bottom:0px;">
    	<div class="bnav_text">
                <div class="hidden-xs">
                    <button type="button" class="btn btn-link pull-left" onclick="window.location.href='../index.html'"><span class="entypo-home" style="padding-right: 20px;"></span>HOMEPAGE</button>
                    <div class="pull-left linevertical"></div>
                    <button type="button" class="btn btn-link pull-left" onclick="window.location.href='../portfolio.html'"><span class="entypo-briefcase" style="padding-right: 20px;"></span>PORTFOLIO</button>
                    <button type="button" class="btn btn-info pull-right" onClick="location.href='mailto:valentinrosciano77@gmail.com'"><span class="entypo-user-add" style="padding-right: 20px;"></span>HIRE ME</button>
                </div>
                <div class="visible-xs">
                    <button type="button" class="btn btn-link pull-left" onclick="window.location.href='../index.html'"><span class="entypo-home"></span></button>
                    <div class="pull-left linevertical"></div>
                    <button type="button" class="btn btn-link pull-left" onclick="window.location.href='../portfolio.html'"><span class="entypo-briefcase"></span></button>
                    <button type="button" class="btn btn-info pull-right" onclick="window.location.href='mailto:valentinrosciano77@gmail.com'"><span class="entypo-user-add"></span></button>
                </div>
            </div>
    </section>
      
    
    <script src="../js/bootstrap.min.js"></script>
   
	<script type="text/javascript" src="../js/skrollr.min.js"></script>
    <script type="text/javascript" src="../js/skrollr.menu.min.js" type="text/javascript" charset="utf-8"></script>
    
    <script type="text/javascript">
		//http://detectmobilebrowsers.com/ + tablets
		(function(a) {
			if(/android|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(ad|hone|od)|iris|kindle|lge |maemo|meego.+mobile|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino|playbook|silk/i.test(a)
			||
			/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))
			{
				//.
				document.write('<script type="text/javascript" src="../js/skrollr.mobile.min.js"><\/script>');
			}
		})(navigator.userAgent||navigator.vendor||window.opera);
    </script>
    <!--[if lt IE 9]>
    <script type="js/javascript" src="dist/skrollr.ie.min.js"></script>
    <![endif]-->
    
    <script type="text/javascript">
        setTimeout(function() {
            var s = skrollr.init();
            
            skrollr.menu.init(s, {
            animate: true,
            duration: 2000,
            easing: 'sqrt'
            })
            
        }, 500);
    </script>
    
    <!-- Preloader -->
	<script type="text/javascript">
        //<![CDATA[
            $(window).load(function() { // makes sure the whole site is loaded
                $('#status').fadeOut(); // will first fade out the loading animation
                $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
                $('body').delay(350).css({'overflow':'visible'});
            })
        //]]>
    </script>
    
</body>
</html>
